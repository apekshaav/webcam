
// WebcamDlg.cpp : implementation file
//

#include "stdafx.h"
#include "Webcam.h"
#include "WebcamDlg.h"
#include "afxdialogex.h"

#include "MyButton.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif


// CAboutDlg dialog used for App About

class CAboutDlg : public CDialogEx
{
public:
	CAboutDlg();

// Dialog Data
#ifdef AFX_DESIGN_TIME
	enum { IDD = IDD_ABOUTBOX };
#endif

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

// Implementation
protected:
	DECLARE_MESSAGE_MAP()
};

CAboutDlg::CAboutDlg() : CDialogEx(IDD_ABOUTBOX)
{
}

void CAboutDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
}

BEGIN_MESSAGE_MAP(CAboutDlg, CDialogEx)
END_MESSAGE_MAP()


// CWebcamDlg dialog



CWebcamDlg::CWebcamDlg(CWnd* pParent /*=NULL*/)
	: CDialogEx(IDD_WEBCAM_DIALOG, pParent)
{
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);

	m_begin = false;
	m_frames_ready = false;

	m_raw_frame = new WFrame;
	m_frame = new WFrame;
	m_output_frame = new WFrame;
}

void CWebcamDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_GL_PREVIEW, m_preview_area);
	DDX_Control(pDX, IDC_START, m_button);
}

void CWebcamDlg::camera_callback(void * arg, void * image_data, uint32_t width, uint32_t height)
{
	CWebcamDlg * pThis = (CWebcamDlg*)arg;

	std::lock_guard<std::mutex> lock(pThis->m_image_mutex);

	WFrame* w_frame;
	w_frame = pThis->GetRawFrame();
	w_frame->width = width;
	w_frame->height = height;
	w_frame->size = width * height * 2;

	if (w_frame->frame == NULL)
		w_frame->frame = new uint8_t[w_frame->size];

	memcpy(w_frame->frame, (unsigned char *)(image_data), w_frame->size);

	pThis->m_begin = true;
}

DWORD WINAPI CWebcamDlg::ProcessFrames(void* arg)
{
	CWebcamDlg * pThis = (CWebcamDlg*)arg;

	while (pThis->m_begin)
	{
		cv::Mat img, yuv;
		WFrame* w_raw_frame;
		WFrame* w_frame;
		{
			std::lock_guard<std::mutex> lock(pThis->m_image_mutex);
			
			w_raw_frame = pThis->GetRawFrame();
			w_frame = pThis->GetFrame();

			if (w_frame->frame == NULL)
			{
				w_frame->width = w_raw_frame->width;
				w_frame->height = w_raw_frame->height;
				w_frame->size = w_frame->width * w_frame->height * 3;
				w_frame->frame = new uint8_t[w_frame->size];
			}

			img = cv::Mat(w_raw_frame->height, w_raw_frame->width, CV_8UC2, w_raw_frame->frame);
		}

		cv::cvtColor(img, yuv, cv::COLOR_YUV2RGB_YUY2);

		{
			std::lock_guard<std::mutex> lock(pThis->m_image_mutex);

			memcpy(w_frame->frame, yuv.data, w_frame->size);
		}

		pThis->m_frames_ready = true;		
	}

	return 0;

}


void CWebcamDlg::DrawLoop()
{
	if (m_frames_ready)
	{
		std::lock_guard<std::mutex> lock(m_image_mutex);

		if (m_frame->frame)
		{
			m_scene_renderer->HandleInputFrame(m_cdc, m_frame);
		}

		m_scene_renderer->DrawScene(m_cdc);

		// Send to DeckLink
		if (m_deckOut)
		{
			WFrame* w_frame;
			w_frame = GetOutputFrame();

			if (w_frame->frame == NULL)
			{
				w_frame->height = DL_OUTPUT_FRAME_HEIGHT;
				w_frame->width = DL_OUTPUT_FRAME_WIDTH;
				w_frame->size = DL_OUTPUT_FRAME_SIZE;
				w_frame->frame = new uint8_t[DL_OUTPUT_FRAME_SIZE];
			}

			m_scene_renderer->RenderToFrame(m_cdc, w_frame->frame, 0);

			if (m_deckOut)
			{
				m_deckOut->UpdateBuffers(m_output_frame->frame, DL_OUTPUT_FRAME_SIZE);
			}
			
		}

	}
	
}


BEGIN_MESSAGE_MAP(CWebcamDlg, CDialogEx)
	ON_WM_SYSCOMMAND()
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	ON_WM_TIMER()

	ON_WM_CLOSE()
	ON_BN_CLICKED(IDC_START, &CWebcamDlg::OnBnClickedStart)
	ON_BN_CLICKED(IDC_EXIT, &CWebcamDlg::OnBnClickedExit)
END_MESSAGE_MAP()


// CWebcamDlg message handlers

BOOL CWebcamDlg::OnInitDialog()
{
	CDialogEx::OnInitDialog();

	// Add "About..." menu item to system menu.

	// IDM_ABOUTBOX must be in the system command range.
	ASSERT((IDM_ABOUTBOX & 0xFFF0) == IDM_ABOUTBOX);
	ASSERT(IDM_ABOUTBOX < 0xF000);

	CMenu* pSysMenu = GetSystemMenu(FALSE);
	if (pSysMenu != NULL)
	{
		BOOL bNameValid;
		CString strAboutMenu;
		bNameValid = strAboutMenu.LoadString(IDS_ABOUTBOX);
		ASSERT(bNameValid);
		if (!strAboutMenu.IsEmpty())
		{
			pSysMenu->AppendMenu(MF_SEPARATOR);
			pSysMenu->AppendMenu(MF_STRING, IDM_ABOUTBOX, strAboutMenu);
		}
	}

	// Set the icon for this dialog.  The framework does this automatically
	//  when the application's main window is not a dialog
	SetIcon(m_hIcon, TRUE);			// Set big icon
	SetIcon(m_hIcon, FALSE);		// Set small icon

	ShowWindow(SW_MAXIMIZE) ;

	/// OpenGL Rendering
	// Setting Preview Area
	m_cdc = m_preview_area.GetDC();

	m_scene_renderer = new ModelRenderer();

	bool result;
	result = m_scene_renderer->CreateGLContext(m_cdc);
	if (!result)
		return false;

	m_scene_renderer->PrepareScene(m_cdc);

	CRect rect;
	m_preview_area.GetWindowRect(&rect);
	int width = rect.right - rect.left;
	int height = rect.bottom - rect.top;
	m_scene_renderer->Reshape(m_cdc, width, height);

	/// Setup Decklink
	m_deckOut = m_decklink.InitOutputAtPort(DECK_VIDEO_OUT, BGRA);

	if (m_decklink.IsInitialized())
	{
		if (m_deckOut != NULL)
			m_deckOut->Start();
	}

	// Connect to camera
	m_camera.RegisterVideoCallback(this, camera_callback);
	m_camera.EnumerateCamera("USB Camera");
	m_camera.StartCapture();

	// Start draw loop timer trigger
	SetTimer(0, 20, NULL);

	return TRUE;  // return TRUE  unless you set the focus to a control
}

void CWebcamDlg::OnSysCommand(UINT nID, LPARAM lParam)
{
	if ((nID & 0xFFF0) == IDM_ABOUTBOX)
	{
		CAboutDlg dlgAbout;
		dlgAbout.DoModal();
	}
	else
	{
		CDialogEx::OnSysCommand(nID, lParam);
	}
}

// If you add a minimize button to your dialog, you will need the code below
//  to draw the icon.  For MFC applications using the document/view model,
//  this is automatically done for you by the framework.

void CWebcamDlg::OnPaint()
{
	CPaintDC dc(this); // device context for painting
	CRect rect;
	GetClientRect(&rect);

	CBrush background_colour;
	background_colour.CreateSolidBrush(RGB(0, 0, 0));
	dc.FillRect(&rect, &background_colour);

	if (IsIconic())
	{
		SendMessage(WM_ICONERASEBKGND, reinterpret_cast<WPARAM>(dc.GetSafeHdc()), 0);

		// Center icon in client rectangle
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// Draw the icon
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialogEx::OnPaint();
	}
}

// The system calls this function to obtain the cursor to display while the user drags
//  the minimized window.
HCURSOR CWebcamDlg::OnQueryDragIcon()
{
	return static_cast<HCURSOR>(m_hIcon);
}

void CWebcamDlg::OnTimer(UINT_PTR nIDEvent)
{
	DrawLoop();

	CDialogEx::OnTimer(nIDEvent);
}

void CWebcamDlg::OnClose()
{
	m_camera.StopCapture();

	m_scene_renderer->DestroyScene(m_cdc);
	delete m_scene_renderer;
	m_scene_renderer = NULL;

	m_begin = false;

	DWORD exit_code = 0;
	TerminateThread(m_process_handle, exit_code);

	delete m_frame;
	m_frame = NULL;

	delete m_output_frame;
	m_output_frame = NULL;
}



void CWebcamDlg::OnBnClickedStart()
{
	m_process_handle = CreateThread(NULL, 0, ProcessFrames, this, 0, &m_process_thread);
}


void CWebcamDlg::OnBnClickedExit()
{
	OnClose();
}
