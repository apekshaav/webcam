#pragma once

#include <mutex>

#include "GL\glew.h"
#include <glm\glm.hpp>

// Set default texture to be 32bit 1920x1080
#define DEFAULT_TEXTURE_WIDTH	1920
#define DEFAULT_TEXTURE_HEIGHT	1080
#define DEFAULT_TEXTURE_SIZE	DEFAULT_TEXTURE_WIDTH*DEFAULT_TEXTURE_HEIGHT*4
#define DEFAULT_ASPECT_RATIO	DEFAULT_TEXTURE_WIDTH / DEFAULT_TEXTURE_HEIGHT

/*
TextureDimensions Struct
Keeps track of offets required to maintain aspect ratio of texture
*/
struct TextureDimensions {
	glm::vec2 offset;
	glm::vec2 size;
	glm::vec2 scaled_size;

	/*
	Calculate offsets required for desired aspect ratio
	Updates TextureDimensions struct containing offset, original size and scaled size
	*/
	void UpdateTextureDimensions(int scenewidth, int sceneheight, float AR = DEFAULT_ASPECT_RATIO);
};

/*
Generic OpenGL texture rendering class
*/
class TextureRenderer
{
protected:

	// Texture image format
	GLenum	m_image_format;

	GLuint	m_input_texture;
	GLuint	m_texture_buffer;

	GLuint	mProgram;
	GLuint	mFragmentShader;

	// Texture dimensions
	int		m_texture_width;
	int		m_texture_height;
	int		m_texture_size;

	// Mutex for update / draw
	std::mutex m_buffer_mutex;

	// Ensure that textures a setup prior to use
	bool	m_is_initialized;

public:
	TextureRenderer();
	~TextureRenderer();

	/*
	Create and setup texture with given dimensions
	Init textures must be called prior to update / draw calls
	*/
	virtual bool InitTextures(int texturewidth = DEFAULT_TEXTURE_WIDTH, int textureheight = DEFAULT_TEXTURE_HEIGHT, int texturesize = DEFAULT_TEXTURE_SIZE);

	// Update texture with given frame object
	// Frame dimensions must match dimensions specified in texture initialization
	virtual void UpdateTexture(void* frame);

	// Draw texture with given scene dimensions
	// Texture aspect ratio will be preserved
	virtual void DrawTexture(TextureDimensions &context);

	bool IsInitialized() { return m_is_initialized; }

	/*
	Convert YUV 422 format image to RGB
	dst size is width x height x 3 (RGB)

	src, dst, width, height, bytes
	*/
	void static YUV2RGB(uint8_t* src, uint8_t* dst, int width, int height, int bytes);
};

