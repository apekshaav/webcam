#pragma once

#include "GLRenderer.h"
#include "TextureRenderer.h"

class ModelRenderer : public GLRenderer
{
private:
	TextureRenderer*     m_cam_renderer;

public:
	ModelRenderer();
	~ModelRenderer();

	// Override prepareScene for more initialization
	void PrepareScene(CDC* pDC);

private:
	virtual void SetupTextures();
	virtual void SetupLighting();
	virtual void SetupShaders();
	virtual void OnDraw();
	virtual void InputFrameArrived(WFrame* frame);

	/*
	Main draw function
	0 - Center
	1 - Left
	2 - Right
	*/
	void RenderScene(int frameNumber);
};
