#include "stdafx.h"
#include "Camera.h"

#include <vector>

#pragma warning(disable:4996) 

extern "C" {
#include <libavcodec/avcodec.h>
#include <libavdevice/avdevice.h>
#include <libavutil/frame.h>
#include <libavformat/avformat.h>
}

#include <dshow.h>
#pragma comment(lib, "strmiids")

AVFrame *frame = NULL;
AVFormatContext *pFormatCtx = NULL;
AVCodecContext * pCodecCtx = NULL;

DEFINE_GUID(CLSID_VideoInputDeviceCategory, 0x860bb310, 0x5d01, 0x11d0, 0xbd, 0x3b, 0x00, 0xa0, 0xc9, 0x11, 0xce, 0x86);


void(*image_data_callback)(void* arg, void * image_data, uint32_t width, uint32_t height) = NULL;

Camera::Camera()
{
	avdevice_register_all();
	av_register_all();

	camera_position = -1;
	exit_thread = false;

	image_data = NULL;
}

Camera::~Camera()
{
	exit_thread = true;
	DWORD exit_code = 0;

	TerminateThread(image_grab_handle, exit_code);
	Sleep(1000);

	if (image_data != NULL)
		delete[] image_data;
	if (frame != NULL)
		av_free(frame);

	avcodec_close(pCodecCtx);
	avformat_close_input(&pFormatCtx);
}

void Camera::RegisterVideoCallback(void* arg, void(*init_image_data_callback)(void* arg, void * image_data, uint32_t width, uint32_t height))
{
	arguments = arg;
	image_data_callback = init_image_data_callback;
}

DWORD WINAPI Camera::image_grab(void * lpParam)
{
	Camera * pThis = (Camera *)lpParam;
	while (!pThis->exit_thread)
	{
		int res;
		int frameFinished;
		AVPacket packet;

		frame = av_frame_alloc();

		while (res = av_read_frame(pFormatCtx, &packet) >= 0)
		{
			if (packet.stream_index == pThis->camera_position)
			{
				avcodec_decode_video2(pCodecCtx, frame, &frameFinished, &packet);
				if (frameFinished)
				{
					(*image_data_callback)(pThis->arguments, frame->data[0], pThis->width, pThis->height);
				}
			}
			av_free_packet(&packet);
		}
	}

	return 0;
}

bool Camera::EnumerateCamera(std::string camera_name)
{
	pFormatCtx = avformat_alloc_context();
	AVInputFormat *iformat = av_find_input_format("dshow");
	AVCodec * pCodec;
	int videoStream = -1;

	std::string cam_id = "video=";

	cam_id += camera_name;

	if (avformat_open_input(&pFormatCtx, cam_id.c_str(), iformat, NULL) != 0 ||
		avformat_find_stream_info(pFormatCtx, NULL) < 0)
	{
		return false;
	}

	for (unsigned int i = 0; i < pFormatCtx->nb_streams; i++)
	{
		AVStream *stream;
		AVCodecContext *codec_ctx;
		stream = pFormatCtx->streams[i];
		codec_ctx = stream->codec;
		if (camera_position == -1 && codec_ctx->codec_type == AVMEDIA_TYPE_VIDEO)
		{
			camera_position = i;
			pCodecCtx = codec_ctx;
			pCodec = avcodec_find_decoder(pCodecCtx->codec_id);
			if (pCodec == NULL || avcodec_open2(pCodecCtx, pCodec, NULL) < 0)
			{
				camera_position = -1;
			}
		}
	}
	if (camera_position == -1)
		return false;

	width = pCodecCtx->width;
	height = pCodecCtx->height;
	original_size = avpicture_get_size(pCodecCtx->pix_fmt, width, height);

	image_data = new uint8_t[original_size];

	return true;
}

int Camera::GetCameras(std::vector<std::string> * camera_list)
{
	/*HRESULT hr;
	hr = CoInitialize(NULL);

	camera_list->clear();

	ICreateDevEnum *pDevEnum;
	hr = CoCreateInstance(CLSID_SystemDeviceEnum, 0, CLSCTX_INPROC_SERVER, IID_ICreateDevEnum, (void**)&pDevEnum);
	if (FAILED(hr))
		return 0;
	IEnumMoniker *pEnum;
	hr = pDevEnum->CreateClassEnumerator(CLSID_VideoInputDeviceCategory, &pEnum, 0);
	if (FAILED(hr))
	{
		return 0;
	}
	IMoniker *pMoniker = NULL;
	while (S_OK == pEnum->Next(1, &pMoniker, NULL))
	{
		IPropertyBag *pPropBag;
		LPOLESTR str = 0;
		hr = pMoniker->GetDisplayName(0, 0, &str);
		if (SUCCEEDED(hr))
		{
			hr = pMoniker->BindToStorage(0, 0, IID_IPropertyBag, (void**)&pPropBag);
			if (SUCCEEDED(hr))
			{
				VARIANT var;
				VariantInit(&var);
				hr = pPropBag->Read(L"FriendlyName", &var, 0);
				if (SUCCEEDED(hr))
				{
					CString temp;
					temp.Format("%S", var.bstrVal);

					camera_list->push_back(temp.GetBuffer());
					VariantClear(&var);
				}
			}
		}
	}*/
	return 0;
}

void Camera::StartCapture()
{
	exit_thread = false;
	image_grab_handle = CreateThread(NULL, 0, image_grab, this, 0, &image_grab_thread);
}

void Camera::StopCapture()
{
	exit_thread = true;
	DWORD exit_code = 0;

	TerminateThread(image_grab_handle, exit_code);
	Sleep(1000);
}