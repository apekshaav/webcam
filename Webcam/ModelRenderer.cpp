#include "stdafx.h"
#include "ModelRenderer.h"

#include <algorithm>

#define NUM_FBO_FRAMES 1

ModelRenderer::ModelRenderer()
{
	m_cam_renderer = new TextureRenderer();
	
	// TODO: Set this with prepareScene?
	m_frame_width = 1920;
	m_frame_height = 1080;

	// Override FBO frames
	m_num_fbo_frames = NUM_FBO_FRAMES;
}

ModelRenderer::~ModelRenderer()
{
	delete m_cam_renderer;
	m_cam_renderer = NULL;
}

void ModelRenderer::PrepareScene(CDC* pDC)
{
	GLRenderer::PrepareScene(pDC);
}

void ModelRenderer::SetupTextures()
{
	GLRenderer::SetupTextures();
}

void ModelRenderer::SetupLighting()
{
	GLRenderer::SetupLighting();
}

void ModelRenderer::SetupShaders()
{
	GLRenderer::SetupShaders();
}

void ModelRenderer::OnDraw()
{
	// Wrap DrawCombinedModel glBindFramebuffer
	glClearColor(0.0f, 0.0f, 0.0f, 1.0f);	// black background

	for (int i = 0; i < m_num_fbo_frames + 1; i++)
	{
		if (!i)
		{
			// Draw to screen
			glBindFramebuffer(GL_FRAMEBUFFER, 0);
		}
		else
		{
			// Draw to specified FBO
			glBindFramebuffer(GL_FRAMEBUFFER, m_buffer_list[i - 1].fbo->GetHandle());
		}

		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
		RenderScene(i);
	}
}

void ModelRenderer::InputFrameArrived(WFrame* frame)
{
	if (!frame) return;

	TextureRenderer* input_source = m_cam_renderer;

	// Lazy initialize texture renderer to get proper frame dimensions
	if (!input_source->IsInitialized())
	{
		input_source->InitTextures(frame->width, frame->height, frame->size);
	}

	input_source->UpdateTexture(frame->frame);
}

void ModelRenderer::RenderScene(int frameNumber)
{
	TextureDimensions dimensions;
	double scene_height, scene_width;

	// Set proper dimensions
	if (frameNumber)
	{
		// console
		scene_height = m_frame_height;
		scene_width = m_frame_width;
	}
	else
	{
		// screen
		scene_height = m_view_height;
		scene_width = m_view_width;
	}

	dimensions.UpdateTextureDimensions(scene_width, scene_height, 1920.0f / 1080.0f);

	glViewport(0, 0, scene_width, scene_height);
	m_cam_renderer->DrawTexture(dimensions);
		
}
