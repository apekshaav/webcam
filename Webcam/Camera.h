#pragma once
#include <string>
#include <vector>

class Camera
{
public:
	Camera();
	~Camera();

	void RegisterVideoCallback(void* arg, void(*image_data_callback)(void* arg, void * image_data, uint32_t width, uint32_t height));
	void StartCapture();
	void StopCapture();
	bool EnumerateCamera(std::string camera_name);
	int GetCameras(std::vector<std::string> * camera_list);

private:
	int camera_position;
	int width;
	int height;

	void *arguments;

	int original_size;

	uint8_t * image_data;
	DWORD   image_grab_thread;
	HANDLE image_grab_handle;

	unsigned exit_thread;

	static DWORD WINAPI image_grab(void* Param);
};

