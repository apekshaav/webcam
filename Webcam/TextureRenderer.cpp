#include "stdafx.h"
#include "TextureRenderer.h"

#include <fstream>  
#include <iostream>  

#define DEFAULT_IMAGE_FORMAT GL_RGB

TextureRenderer::TextureRenderer()
{
	m_is_initialized = false;
	m_image_format = DEFAULT_IMAGE_FORMAT;
}


TextureRenderer::~TextureRenderer()
{
}

bool TextureRenderer::InitTextures(int texturewidth, int textureheight, int texturesize)
{
	m_texture_width = texturewidth;
	m_texture_height = textureheight;
	m_texture_size = texturesize;

	glGenBuffers(1, &m_texture_buffer);

	glEnable(GL_TEXTURE_2D);
	glGenTextures(1, &m_input_texture);
	glBindTexture(GL_TEXTURE_2D, m_input_texture);

	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP);

	// Create texture with empty data, we will update it using glTexSubImage2D each frame.
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, texturewidth, textureheight, 0, m_image_format, GL_UNSIGNED_BYTE, NULL);
	glBindTexture(GL_TEXTURE_2D, 0);

	m_is_initialized = true;

	return true;
}


void TextureRenderer::UpdateTexture(void* frame)
{
	if (!m_is_initialized) return;

	std::lock_guard<std::mutex> lock(m_buffer_mutex);

	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	glEnable(GL_TEXTURE_2D);

	// Use a straightforward texture buffer
	glBindBuffer(GL_PIXEL_UNPACK_BUFFER, m_texture_buffer);
	glBufferData(GL_PIXEL_UNPACK_BUFFER, m_texture_size, frame, GL_DYNAMIC_DRAW);
	glBindTexture(GL_TEXTURE_2D, m_input_texture);

	// NULL for last arg indicates use current GL_PIXEL_UNPACK_BUFFER target as texture data
	glTexSubImage2D(GL_TEXTURE_2D, 0, 0, 0, m_texture_width, m_texture_height, m_image_format, GL_UNSIGNED_BYTE, NULL);

	glBindTexture(GL_TEXTURE_2D, 0);
	glBindBuffer(GL_PIXEL_UNPACK_BUFFER, 0);
	glDisable(GL_TEXTURE_2D);
}

void TextureRenderer::DrawTexture(TextureDimensions &context)
{
	if (!m_is_initialized) return;

	std::lock_guard<std::mutex> lock(m_buffer_mutex);

	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	glOrtho(0.0, context.size.x, context.size.y, 0.0, 1.0, -1.0);
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glFinish();

	glEnable(GL_TEXTURE_2D);
	glBindTexture(GL_TEXTURE_2D, m_input_texture);

	int x_offset = (int)context.offset.x;
	int y_offset = (int)context.offset.y;

	int texture_width = (int)context.scaled_size.x;
	int texture_height = (int)context.scaled_size.y;

	glBegin(GL_QUADS);
	glTexCoord2i(0, 0); glVertex2i(x_offset, y_offset);
	glTexCoord2i(1, 0); glVertex2i(x_offset + texture_width, y_offset);
	glTexCoord2i(1, 1); glVertex2d(x_offset + texture_width, y_offset + texture_height);
	glTexCoord2i(0, 1); glVertex2d(x_offset, y_offset + texture_height);
	glEnd();

	glDisable(GL_TEXTURE_2D);
	glBindTexture(GL_TEXTURE_2D, 0);

	glMatrixMode(GL_TEXTURE);
	glLoadIdentity();
}

////////////////////////////// Struct Functions //////////////////////////////

void TextureDimensions::UpdateTextureDimensions(int scenewidth, int sceneheight, float AR)
{
	float width = scenewidth;
	float height = (float)width / AR;

	offset.x = 0.0f;
	offset.y = 0.0f;

	if (height > sceneheight)
	{
		height = (float)sceneheight;
		width = height * AR;

		offset.x = (scenewidth - width) * 0.5f;
	}
	else
	{
		offset.y = (sceneheight - height) * 0.5f;
	}

	size.x = scenewidth;
	size.y = sceneheight;

	scaled_size.x = width;
	scaled_size.y = height;
}

////////////////////////////// Static Functions //////////////////////////////

#define CLIP(X) ( (X) > 255 ? 255 : (X) < 0 ? 0 : X)

// YUV -> RGB
#define C(Y) ( (Y) - 16  )
#define D(U) ( (U) - 128 )
#define E(V) ( (V) - 128 )

#define YUV2R(Y, U, V) CLIP(( 298 * C(Y)              + 409 * E(V) + 128) >> 8)
#define YUV2G(Y, U, V) CLIP(( 298 * C(Y) - 100 * D(U) - 208 * E(V) + 128) >> 8)
#define YUV2B(Y, U, V) CLIP(( 298 * C(Y) + 516 * D(U)              + 128) >> 8)

void TextureRenderer::YUV2RGB(uint8_t* src, uint8_t* dst, int width, int height, int bytes)
{
	for (int i = 0, c = 0; i < width*height*bytes; i += 4, c += 6)
	{
		int U = src[i];
		int Y1 = src[i + 1];
		int V = src[i + 2];
		int Y2 = src[i + 3];

		dst[c] = YUV2R(Y1, U, V);
		dst[c + 1] = YUV2G(Y1, U, V);
		dst[c + 2] = YUV2B(Y1, U, V);
		dst[c + 3] = YUV2R(Y2, U, V);
		dst[c + 4] = YUV2G(Y2, U, V);
		dst[c + 5] = YUV2B(Y2, U, V);
	}
}