#pragma once

#include <GL/glew.h>
#include <gl/glu.h>

#include <vector>

#include "Texture.h"
#include "FramebufferObject.h"

// Struct for OpenGLLib objects
typedef struct PBufferObject {
	std::shared_ptr<Texture> texture{ nullptr };
	std::shared_ptr<FramebufferObject> fbo{ nullptr };
} PBufferObject;

// Container for frame parameters
struct WFrame
{
	uint8_t *frame = NULL;

	int width;
	int height;
	int size;
	int source;

	~WFrame()
	{
		if (frame != NULL) {
			delete[] frame;
			frame = NULL;
		}
	}
};

/*
MFC based OpenGL renderer with DL functions
Contains methods for initialization and rendering
Wraps functions with wglMakeCurrent to simplify rendering process
*/

class GLRenderer
{
protected:

	HDC		m_hDC;

	// Current rendering context
	HGLRC	m_hRC;

	// List of buffer objects used to render to screen and DL
	std::vector<PBufferObject> m_buffer_list;

	// FBO frame dimensions
	int		m_frame_width;
	int		m_frame_height;

	// Number of frames required, varies based on number of DL outputs required
	int		m_num_fbo_frames;

	// Scene dimensions (GUI application)
	int		m_view_width;
	int		m_view_height;

public:
	GLRenderer();
	~GLRenderer();

	/*
	Main Renderer functions
	External functions for renderer interaction
	*/

	// Setup GL Context, must be called first
	bool CreateGLContext(CDC* pDC);

	// Setup scene, (textures, lighting, etc)
	void PrepareScene(CDC* pDC);

	// Destroy setup for deinit
	void DestroyScene(CDC* pDC);

	// Resize current viewing area, updates view dimensions
	void Reshape(CDC* pDC, int w, int h);

	// Update current scene
	void DrawScene(CDC* pDC);

	/*
	DeckLink Functions
	*/

	// Copy scene from specified frame buffer to frame object
	void RenderToFrame(CDC* pDC, void* frame, int frameNumber);

	// Handle given input frame
	void HandleInputFrame(CDC* pDC, WFrame* frame);

protected:

	/*
	Internal wrapped functions
	Wrapped functions are virtual and need to be overriden by subclasses
	*/

	// Called by PrepareScene
	virtual void SetupTextures() {}

	// Called by PrepareScene
	virtual void SetupLighting() {}

	// Called by PrepareScene
	virtual void SetupShaders() {}

	// Called by PrepareScene
	virtual bool SetupFBO();

	// Called by DrawScene
	virtual void OnDraw() {}

	// Called by HandleInputFrame
	virtual void InputFrameArrived(WFrame* frame) {}
};

