
// WebcamDlg.h : header file
//

#pragma once

#include "opencv2/opencv.hpp"
#include "opencv2/highgui/highgui.hpp"
#include "opencv2/highgui/highgui_c.h"

#include <mutex>

#include "Camera.h"
#include "afxwin.h"

// OpenGL Rendering
#include "ModelRenderer.h"

// Decklink
#include "DeckLinkCap.h"
#include "MyButton.h"

#define DECK_VIDEO_OUT		1

#define DL_OUTPUT_FRAME_WIDTH	1920
#define DL_OUTPUT_FRAME_HEIGHT	1080
#define DL_OUTPUT_FRAME_SIZE	DL_OUTPUT_FRAME_WIDTH*DL_OUTPUT_FRAME_HEIGHT*4

#define BGRA	0
#define YUV		1


// CWebcamDlg dialog
class CWebcamDlg : public CDialogEx
{
// Construction
public:
	CWebcamDlg(CWnd* pParent = NULL);	// standard constructor

// Dialog Data
#ifdef AFX_DESIGN_TIME
	enum { IDD = IDD_WEBCAM_DIALOG };
#endif

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV support

private:
	std::mutex	m_image_mutex;
	
	// Webcam
	Camera		m_camera;
	WFrame*		m_raw_frame;
	WFrame*		m_frame;
	bool		m_begin;
	bool		m_frames_ready;

	DWORD		m_process_thread;
	HANDLE		m_process_handle;
	
	// Draw context
	CDC*			m_cdc;
	ModelRenderer*	m_scene_renderer;

	// Decklink
	DeckLinkCap				m_decklink;
	DeckLinkOutputDevice*	m_deckOut;
	WFrame*					m_output_frame;

	static void camera_callback(void* arg, void * image_data, uint32_t width, uint32_t height);
	static DWORD WINAPI ProcessFrames(void* arg);

	void	DrawLoop();

	WFrame* GetRawFrame() { return m_raw_frame; }
	WFrame* GetFrame() { return m_frame; }
	WFrame* GetOutputFrame() { return m_output_frame; }

// Implementation
protected:
	HICON m_hIcon;

	// Generated message map functions
	virtual BOOL OnInitDialog();
	afx_msg void OnSysCommand(UINT nID, LPARAM lParam);
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	afx_msg void OnTimer(UINT_PTR nIDEvent);
	DECLARE_MESSAGE_MAP()

public:
	afx_msg void OnClose();
	CStatic m_preview_area;
	afx_msg void OnBnClickedStart();
	afx_msg void OnBnClickedExit();
	CMyButton m_button;
};
