#include "stdafx.h"
#include "GLRenderer.h"


GLRenderer::GLRenderer()
{
	m_hRC = NULL;

	m_view_width = -1;
	m_view_height = -1;

	m_frame_width = -1;
	m_frame_height = -1;

	// Default to a single FBO frame
	// Override if necessary
	m_num_fbo_frames = 1;
}

GLRenderer::~GLRenderer()
{
}

// Setup GLEW
// Creates rendering context and pixel format
/// <returns>
/// Successful or unsuccessful inialization
/// </returns>
bool GLRenderer::CreateGLContext(CDC* pDC)
{
	PIXELFORMATDESCRIPTOR pfd =
	{
		sizeof(PIXELFORMATDESCRIPTOR),	// size of this pfd
		1,								// version number
		PFD_DRAW_TO_WINDOW |            // support window
		PFD_SUPPORT_OPENGL |            // support OpenGL
		PFD_DOUBLEBUFFER,               // double buffered
		PFD_TYPE_RGBA,                  // RGBA type
		24,                             // 24-bit color depth
		0, 0, 0, 0, 0, 0,               // color bits ignored
		1,                              // no alpha buffer
		0,                              // shift bit ignored
		0,                              // no accumulation buffer
		0, 0, 0, 0,                     // accum bits ignored
		24,                             // 32-bit z-buffer
		0,                              // no stencil buffer
		0,                              // no auxiliary buffer
		PFD_MAIN_PLANE,                 // main layer
		0,                              // reserved
		0, 0, 0                         // layer masks ignored
	};

	m_hDC = pDC->m_hDC;

	// Pixel format
	int iPixelFormat = ChoosePixelFormat(pDC->m_hDC, &pfd);
	if (iPixelFormat == 0) return false;

	// Set pixel format
	bool result = SetPixelFormat(pDC->m_hDC, iPixelFormat, &pfd);
	if (!result) return false;

	m_hRC = wglCreateContext(pDC->m_hDC);
	wglMakeCurrent(pDC->m_hDC, m_hRC);

	GLenum err = glewInit();

	if (GLEW_OK != err)
	{
		AfxMessageBox(_T("GLEW is not initialized!"));
		return false;
	}

	const GLubyte *GLver = glGetString(GL_VERSION);

	if (!strncmp((char*)GLver, "1", 1)) return false;

	if (!m_hRC) return false;

	return true;
}

void GLRenderer::PrepareScene(CDC* pDC)
{
	// Set color to use when clearing the background.
	float bgColor = 0.0f;
	glClearColor(bgColor, bgColor, 0.5, 1.0f);
	glClearDepth(1.0f);

	// Turn on backface culling
	glFrontFace(GL_CCW);
	glCullFace(GL_BACK);

	// Turn on depth testing
	glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_LEQUAL);

	SetupTextures();
	SetupLighting();
	SetupShaders();
	SetupFBO();
}

void GLRenderer::DestroyScene(CDC* pDC)
{
	wglMakeCurrent(NULL, NULL);
	if (m_hRC)
	{
		wglDeleteContext(m_hRC);
		m_hRC = NULL;
	}
}

void GLRenderer::Reshape(CDC* pDC, int w, int h)
{
	m_view_width = w;
	m_view_height = h;
}

void GLRenderer::DrawScene(CDC* pDC)
{
	wglMakeCurrent(pDC->m_hDC, m_hRC);

	OnDraw();

	SwapBuffers(pDC->m_hDC);

	wglMakeCurrent(NULL, NULL);
}

bool GLRenderer::SetupFBO()
{
	// Add required number of buffer objects to list
	for (int i = 0; i < m_num_fbo_frames; i++)
	{
		// Objects are created with required frame dimensions
		PBufferObject buffer;
		buffer.texture = std::make_shared<Texture>(GL_TEXTURE_2D, nullptr, m_frame_width, m_frame_height, 0, GL_RGBA, GL_BGRA, 0, 0, GL_UNSIGNED_BYTE);
		buffer.fbo = std::make_shared<FramebufferObject>();
		buffer.fbo->AttachTexture2D(buffer.texture->GetHandle());
		buffer.fbo->AttachRenderbuffer(FramebufferObject::RenderBufferAttachement::DEPTH, GL_DEPTH_COMPONENT, m_frame_width, m_frame_height);
		buffer.fbo->Validate();

		m_buffer_list.push_back(buffer);
	}

	return true;
}

// TODO: Find more elegant solution for frame number offset
void GLRenderer::RenderToFrame(CDC* pDC, void* frame, int frameNumber)
{
	// make GL context current in this thread
	wglMakeCurrent(pDC->m_hDC, m_hRC);

	glBindFramebuffer(GL_FRAMEBUFFER, m_buffer_list[frameNumber].fbo->GetHandle());
	glReadPixels(0, 0, m_frame_width, m_frame_height, GL_BGRA, GL_UNSIGNED_BYTE, frame);

	wglMakeCurrent(NULL, NULL);
}

void GLRenderer::HandleInputFrame(CDC* pDC, WFrame* frame)
{
	wglMakeCurrent(pDC->m_hDC, m_hRC);

	InputFrameArrived(frame);

	wglMakeCurrent(NULL, NULL);
}