#pragma once

#include "DeckLinkAPI_h.h"

#include "DeckLinkInputDevice.h"
#include "DeckLinkOutputDevice.h"

#ifdef BLACKMAGICCAPTURE_EXPORTS  
#define BLACKMAGICCAPTURE_API __declspec(dllexport)   
#else  
#define BLACKMAGICCAPTURE_API __declspec(dllimport)   
#endif 

class BLACKMAGICCAPTURE_API DeckLinkCap
{
public:
	DeckLinkCap();
	virtual ~DeckLinkCap();

	DeckLinkInputDevice* InitInputAtPort(int port, BMDDisplayMode displayMode = DEFAULT_DISPLAY_MODE);
	DeckLinkOutputDevice* InitOutputAtPort(int port, int format);
	bool RemoveDeviceAtPort(int port);


	bool IsInitialized() { return m_initialized; }

private:
	IDeckLink*	m_deckLink[16];

	DeckLinkDevice*	m_deckLinkDeviceList[8] = { 0 };

	bool		m_initialized;
};