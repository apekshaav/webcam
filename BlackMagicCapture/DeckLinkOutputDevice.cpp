#include <comutil.h>
#include "DeckLinkOutputDevice.h"

using namespace std;

// Video mode parameters
DeckLinkOutputDevice::DeckLinkOutputDevice(IDeckLink* dl, int port, int format)
{
	m_deckLinkOutput = NULL;

	kDisplayMode = bmdModeHD1080i5994; //bmdModeHD1080p2398
	kOutputFlags = bmdVideoOutputFlagDefault;

	kFrameDuration = 0;
	kTimeScale = 0;

	kFrameWidth = 1920;
	kFrameHeight = 1080;

	stop = 0;

	if (format == 1)
	{
		kPixelFormat = bmdFormat8BitYUV;
		kRowBytes = kFrameWidth * 2;
	}
	else
	{
		kPixelFormat = bmdFormat8BitBGRA;
		kRowBytes = kFrameWidth * 4;
	}

	m_frame_width = kFrameWidth;
	m_frame_height = kFrameHeight;

	rd_buffer_count = 0;
	wr_buffer_count = 0;

	videoFrameBlue = NULL;
	previousImage = NULL;

	gTotalFramesScheduled = 0;

	m_port = port;

	HRESULT result;
	result = dl->QueryInterface(IID_IDeckLinkOutput, (void**)&m_deckLinkOutput);
	if (result != S_OK) return;
}

DeckLinkOutputDevice::~DeckLinkOutputDevice()
{
	stop = 1;
	if (m_deckLinkOutput != NULL)
	{
		Stop();

		m_deckLinkOutput->Release();
		m_deckLinkOutput = NULL;
	}

	if (previousImage != NULL)
		delete[] previousImage;

	if (videoFrameBlue != NULL)
		videoFrameBlue->Release();

	if (buffers)
	{
		for (int i = 0; i < IMAGE_BUFFER_COUNT; i++)
		{
			if (buffers[i].buffer) buffers[i].buffer->Release();
		}
	}
}

void DeckLinkOutputDevice::Start()
{
	// Create a frame with defined format
	if (!videoFrameBlue)
	{
		videoFrameBlue = CreateFrame(m_deckLinkOutput);
	}
	stop = 0;
	for (int i = 0; i < IMAGE_BUFFER_COUNT; i++)
	{
		if (!buffers[i].buffer)
		{
			buffers[i].buffer = CreateFrame(m_deckLinkOutput);
		}
		buffers[i].dirty = 1;
	}

	m_deckLinkOutput->SetScheduledFrameCompletionCallback(this);
	m_deckLinkOutput->EnableVideoOutput(kDisplayMode, kOutputFlags);
	m_deckLinkOutput->ScheduleVideoFrame(videoFrameBlue, gTotalFramesScheduled*kFrameDuration, kFrameDuration, kTimeScale);
	m_deckLinkOutput->StartScheduledPlayback(0, kTimeScale, 1.0);

	gTotalFramesScheduled++;

}

void DeckLinkOutputDevice::Stop()
{
	m_deckLinkOutput->StopScheduledPlayback(0, NULL, 0);
	m_deckLinkOutput->DisableVideoOutput();
	m_deckLinkOutput->SetScheduledFrameCompletionCallback(NULL);

	gTotalFramesScheduled = 0;

	if (videoFrameBlue != NULL)
		videoFrameBlue = NULL;

	if (buffers) {
		for (int i = 0; i < IMAGE_BUFFER_COUNT; i++)
		{
			if (buffers[i].buffer)
			{
				buffers[i].buffer = NULL;
				buffers[i].dirty = 0;
			}
		}
	}
}

bool DeckLinkOutputDevice::UpdateBuffers(unsigned char * image_buffer, unsigned size)
{
	int curr_pos = wr_buffer_count & 1;
	if (buffers[curr_pos].dirty && !stop)
	{
		void * frame;
		buffers[curr_pos].buffer->GetBytes(&frame);

		if (kPixelFormat == bmdFormat8BitYUV && size == kFrameWidth * kFrameHeight * 4)
			return false;
		if (kPixelFormat == bmdFormat8BitBGRA && size == kFrameWidth * kFrameHeight * 2)
			return false;

		memcpy(frame, image_buffer, size);
		buffers[curr_pos].dirty = 0;
		wr_buffer_count++;
		return true;
	}
	return false;
}

IDeckLinkMutableVideoFrame* DeckLinkOutputDevice::CreateFrame(IDeckLinkOutput* deckLinkOutput)
{
	IDeckLinkMutableVideoFrame* frame = NULL;
	if (deckLinkOutput->CreateVideoFrame(kFrameWidth, kFrameHeight, kRowBytes, kPixelFormat, bmdFrameFlagFlipVertical, &frame) == S_OK)
		return frame;

	return NULL;
}

HRESULT	STDMETHODCALLTYPE DeckLinkOutputDevice::ScheduledFrameCompleted(IDeckLinkVideoFrame* completedFrame, BMDOutputFrameCompletionResult result)
{
	// When a video frame completes,reschedule another frame
	void * pFrame;
	completedFrame->GetBytes(&pFrame);

	unsigned row = completedFrame->GetRowBytes();
	unsigned col = completedFrame->GetHeight();

	if (!previousImage)
	{
		previousImage = new unsigned char[row*col];
	}

	void * test_image;
	int rd_pos = rd_buffer_count & 1;

	if (buffers[rd_pos].dirty == 0)
	{
		buffers[rd_pos].buffer->GetBytes(&test_image);
		memcpy(pFrame, test_image, row*col);
		buffers[rd_pos].dirty = 1;
		rd_buffer_count++;
	}
	else
	{
		memcpy(pFrame, previousImage, row*col);
	}

	m_deckLinkOutput->ScheduleVideoFrame(completedFrame, gTotalFramesScheduled*kFrameDuration, kFrameDuration, kTimeScale);

	memcpy(previousImage, pFrame, row*col);
	gTotalFramesScheduled++;
	return S_OK;
}

HRESULT	STDMETHODCALLTYPE DeckLinkOutputDevice::ScheduledPlaybackHasStopped(void)
{
	return S_OK;
}