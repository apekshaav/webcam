
#pragma once 
#include "stdafx.h"
//#include "plyloader.h"
#include "PointCloud.h"
#include "iostream"
//#include "Common.h"
#include <limits>
#include <list>    
/* **********************************************************************************
#  																					#
# Copyright (c) 2015-2016,															#
# Chair for Computer Aided Medical Procedures & Augmented Reality (CAMPAR) I-16		#
# Technische Universit�t M�nchen													#
# 																					#
# All rights reserved.																#
# Felix Bork - felix.bork@tum.de													#
# 																					#
# Redistribution and use in source and binary forms, with or without				#
# modification, are restricted to the following conditions:							#
# 																					#
#  * The software is permitted to be used internally only by CAMPAR and				#
#    any associated/collaborating groups and/or individuals.						#
#  * The software is provided for your internal use only and you may				#
#    not sell, rent, lease or sublicense the software to any other entity			#
#    without specific prior written permission.										#
#    You acknowledge that the software in source form remains a confidential		#
#    trade secret of CAMPAR and therefore you agree not to attempt to				#
#    reverse-engineer, decompile, disassemble, or otherwise develop source			#
#    code for the software or knowingly allow others to do so.						#
#  * Redistributions of source code must retain the above copyright notice,			#
#    this list of conditions and the following disclaimer.							#
#  * Redistributions in binary form must reproduce the above copyright notice,		#
#    this list of conditions and the following disclaimer in the documentation		#
#    and/or other materials provided with the distribution.							#
#  * Neither the name of CAMPAR nor the names of its contributors may be used		#
#    to endorse or promote products derived from this software without				#
#    specific prior written permission.												#
# 																					#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND	#
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED		#
# WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE			#
# DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR	#
# ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES	#
# (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;		#
# LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND		#
# ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT		#
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS		#
# SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.						#
# 																					#
*************************************************************************************/

//vao for point clouds 
PointCloud::PointCloud(const char *filename, bool normal, bool color, bool Faces)
{
	//PLYPointCloud(filename, normal, color);
	//PLYModel PLYPointCloud(filename, normal, color);
	PLYPointCloud = std::make_shared<PLYModel>(filename, normal, color);
	vec_positions = PLYPointCloud->positions;
	vec_indices = PLYPointCloud->vec_indices;

	/*vec_positions.push_back(glm::vec3(1, 0, 0));
	vec_positions.push_back(glm::vec3(0, 0, 0));
	vec_positions.push_back(glm::vec3(1, 1, 0));
	vec_indices.push_back(0); vec_indices.push_back(1); vec_indices.push_back(2);*/

	/*for (int i = 0; i < vec_indices.size(); i++)
	{
		std::cout << " indices " << vec_indices[i] << std::endl;
	}*/

	//glGenVertexArrays(1, &this->vaoHandle);
	//glBindVertexArray(this->vaoHandle);
	//// Positions
	//	this->vboPositionsHandle = GLUtils::BufferUtils::CreateBufferObject(GL_ARRAY_BUFFER, &this->vec_positions[0], this->vec_positions.size() * sizeof(glm::vec3), GL_DYNAMIC_DRAW);
	//	glBindBuffer(GL_ARRAY_BUFFER, this->vboPositionsHandle);
	//		glEnableVertexAttribArray(0);
	//			glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, nullptr);
	//	glBindBuffer(GL_ARRAY_BUFFER, 0);

	//	this->vboIndicesHandle = GLUtils::BufferUtils::CreateBufferObject(GL_ELEMENT_ARRAY_BUFFER, &this->vec_indices[0], vec_indices.size() * sizeof(unsigned int), GL_DYNAMIC_DRAW);
	//	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, this->vboIndicesHandle);

	//glBindVertexArray(0);

	RenderIndices = true;

}

PointCloud::PointCloud(const char *filename, bool normal, bool color) 
{
	//PLYPointCloud(filename, normal, color);
	PLYModel PLYPointCloud(filename, normal, color);
	vec_positions = PLYPointCloud.positions; 

	/*for (int i = 0; i < faceIndexes.size(); i++)
	{
		std::cout << " faceIndexes " << faceIndexes[i].x << " "<< faceIndexes[i].y << " " << faceIndexes[i].z << std::endl;
	}*/

	//std::vector<glm::vec3> vetices = Points->GetVertices();	
	//std::cout << " second one " << vetices[Points->GetVertices().size() - 1].x << " " << vetices[Points->GetVertices().size() - 1].y << " " << vetices[Points->GetVertices().size() - 1].z << std::endl;

	glGenVertexArrays(1, &this->vaoHandle);
	glBindVertexArray(this->vaoHandle);
	// Positions
	this->vboPositionsHandle = GLUtils::BufferUtils::CreateBufferObject(GL_ARRAY_BUFFER, &this->vec_positions[0], this->vec_positions.size() * sizeof(glm::vec3), GL_STATIC_DRAW);
	glBindBuffer(GL_ARRAY_BUFFER, this->vboPositionsHandle);
	glEnableVertexAttribArray(0);
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, nullptr);
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	if ((normal == 1) && (color == 1))
	{
		std::cout << "color : yes , normal = yes" << std::endl;
		vec_normals = PLYPointCloud.normals;
		// Normals
		this->vboNormalsHandle = GLUtils::BufferUtils::CreateBufferObject(GL_ARRAY_BUFFER, &this->vec_normals[0], this->vec_normals.size() * sizeof(glm::vec3), GL_STATIC_DRAW);
		glBindBuffer(GL_ARRAY_BUFFER, this->vboNormalsHandle);
		glEnableVertexAttribArray(1);
		glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 0, nullptr);
		glBindBuffer(GL_ARRAY_BUFFER, 0);

		// Colors
		vec_colors = PLYPointCloud.colors;
		this->vboColorHandle = GLUtils::BufferUtils::CreateBufferObject(GL_ARRAY_BUFFER, &this->vec_colors[0], this->vec_colors.size() * sizeof(glm::vec3), GL_STATIC_DRAW);
		glBindBuffer(GL_ARRAY_BUFFER, this->vboColorHandle);
		glEnableVertexAttribArray(2);
		glVertexAttribPointer(2, 3, GL_FLOAT, GL_FALSE, 0, nullptr);
		glBindBuffer(GL_ARRAY_BUFFER, 0);
	}
	else if ((color == 1) && (normal != 1))
	{
		std::cout << "color : yes , normal = no" << std::endl;
		// Colors
		vec_colors = PLYPointCloud.colors;
		this->vboColorHandle = GLUtils::BufferUtils::CreateBufferObject(GL_ARRAY_BUFFER, &this->vec_colors[0], this->vec_colors.size() * sizeof(glm::vec3), GL_STATIC_DRAW);
		glBindBuffer(GL_ARRAY_BUFFER, this->vboColorHandle);
		glEnableVertexAttribArray(2);
		glVertexAttribPointer(2, 3, GL_FLOAT, GL_FALSE, 0, nullptr);
		glBindBuffer(GL_ARRAY_BUFFER, 0);
	}

	glBindVertexArray(0);

	/*for (int k = 0; k < positions.size(); k++)
	{
	std::cout << " positions " << positions[k].x << " "<< positions[k].y << " " << positions[k].z << std::endl;
	}
	*/
	// Computes the AABB by retrieving min\max values in each axis
	float minX = std::numeric_limits<float>::max(); float maxX = -std::numeric_limits<float>::max();
	float minY = std::numeric_limits<float>::max(); float maxY = -std::numeric_limits<float>::max();
	float minZ = std::numeric_limits<float>::max(); float maxZ = -std::numeric_limits<float>::max();
	for (int i = 0; i < vec_positions.size(); i++)
	{
		glm::vec3 v = vec_positions.at(i);

		//std::cout << "vertex " << i << " " << v.x << " " << v.y << " " << v.z << std::endl; 
		if (minX > v.x) minX = v.x;
		if (maxX < v.x) maxX = v.x;
		if (minY > v.y) minY = v.y;
		if (maxY < v.y) maxY = v.y;
		if (minZ > v.z) minZ = v.z;
		if (maxZ < v.z) maxZ = v.z;
	}

	vec_aabb = std::vector<glm::vec3>();
	vec_aabb.push_back(glm::vec3(minX, minY, minZ));
	vec_aabb.push_back(glm::vec3(maxX, maxY, maxZ));
	m_sideLength = glm::vec3(std::abs(maxX - minX), std::abs(maxY - minY), std::abs(maxZ - minZ));
	m_centroid = glm::vec3((maxX + minX) / 2, (maxY + minY) / 2, (maxZ + minZ) / 2);
	m_costumaabb = vec_aabb;

	std::cout << " range inside min and max " << minX << " " << minY << " " << minZ << std::endl <<
		" max " << maxX << " " << maxY << " " << maxZ << std::endl; 
	//minmax from plyModel object
	range.push_back(PLYPointCloud.min);
	range.push_back(PLYPointCloud.max);
}

//PointCloud::PointCloud(std::vector<glm::vec3> positions)
//{
//	
//}

auto PointCloud::GetIndices()->std::vector<unsigned int>
{
	return vec_indices; 
}

PointCloud::~PointCloud(void)
{
	glDeleteVertexArrays(1, &this->vaoHandle);

	GLUtils::BufferUtils::DeleteBufferObject(&vboPositionsHandle);
	GLUtils::BufferUtils::DeleteBufferObject(&vboNormalsHandle);
	GLUtils::BufferUtils::DeleteBufferObject(&vboTextCoordsHandle);
	GLUtils::BufferUtils::DeleteBufferObject(&vboIndicesHandle);

}

//format -> RequiredScaleRange = (glm::vec3 min , glm::vec3 max)
auto PointCloud::CalculateNewRange(std::vector<glm::vec3>RequiredScaleRange, std::vector<glm::vec3>CurrentRange) -> glm::mat4
{
	glm::mat4 RangeMat; 
	double scale1 = (RequiredScaleRange[1].x - RequiredScaleRange[0].x) / (CurrentRange[1].x - CurrentRange[0].x);
	double scaleY = (RequiredScaleRange[1].y - RequiredScaleRange[0].y) / (CurrentRange[1].y - CurrentRange[0].y);
	double scaleZ = (RequiredScaleRange[1].z - RequiredScaleRange[0]).z / (CurrentRange[1].z - CurrentRange[0].z);

	RangeMat[0][0] = scale1;
	RangeMat[0][1] = 0;
	RangeMat[0][2] = 0;
	RangeMat[0][3] = (-scale1 * CurrentRange[0].x) + RequiredScaleRange[0].x;
	RangeMat[1][0] = 0;
	RangeMat[1][1] = scaleY;
	RangeMat[1][2] = 0;
	RangeMat[1][3] = (-(RequiredScaleRange[1].y - RequiredScaleRange[0].y) / (CurrentRange[1].y - CurrentRange[0].y) * CurrentRange[0].y) + RequiredScaleRange[0].y;
	RangeMat[2][0] = 0;
	RangeMat[2][1] = 0;
	RangeMat[2][2] = (RequiredScaleRange[1].z - RequiredScaleRange[0].z) / (CurrentRange[1].z - CurrentRange[0].z);
	RangeMat[2][3] = (-(RequiredScaleRange[1].z - RequiredScaleRange[0].z) / (CurrentRange[1].z - CurrentRange[0].z) * CurrentRange[0].z) + RequiredScaleRange[0].z;
	RangeMat[3][0] = 0;
	RangeMat[3][1] = 0;
	RangeMat[3][2] = 0;
	RangeMat[3][3] = 1;

	RangeMat[0][0] = scale1;
	RangeMat[0][1] = 0;
	RangeMat[0][2] = 0;
	RangeMat[0][3] = (-scale1 * CurrentRange[0].x) + RequiredScaleRange[0].x;
	RangeMat[1][0] = 0;
	RangeMat[1][1] = (RequiredScaleRange[1].y - RequiredScaleRange[0].y) / (CurrentRange[1].y - CurrentRange[0].y);
	RangeMat[1][2] = 0;
	RangeMat[1][3] = (-(RequiredScaleRange[1].y - RequiredScaleRange[0].y) / (CurrentRange[1].y - CurrentRange[0].y) * CurrentRange[0].y) + RequiredScaleRange[0].y;
	RangeMat[2][0] = 0;
	RangeMat[2][1] = 0;
	RangeMat[2][2] = (RequiredScaleRange[1].z - RequiredScaleRange[0].z) / (CurrentRange[1].z - CurrentRange[0].z);
	RangeMat[2][3] = (-(RequiredScaleRange[1].z - RequiredScaleRange[0].z) / (CurrentRange[1].z - CurrentRange[0].z) * CurrentRange[0].z) + RequiredScaleRange[0].z;
	RangeMat[3][0] = 0;
	RangeMat[3][1] = 0;
	RangeMat[3][2] = 0;
	RangeMat[3][3] = 1;

	return glm::transpose(RangeMat); 
}

auto PointCloud::GetVertices()->std::vector<glm::vec3>
{
	return vec_positions;
}
auto PointCloud::GetColors()->std::vector<glm::vec3>
{
	return vec_colors; 
}

auto PointCloud::GetNormamls()->std::vector<glm::vec3>
{
	return vec_normals; 
}

auto PointCloud::GetAABB() const -> std::vector<glm::vec3>
{
	return vec_aabb;
	//return range; 
}

auto PointCloud::GetBoundingSphere() const -> const std::shared_ptr<Sphere>
{
	return m_boundingSphere;
}

auto PointCloud::CalculateBoundingSphere() -> void
{
	// Calculate bounding sphere
	std::list<std::vector<float>> lp;
	for (int i = 0; i < vec_positions.size(); ++i)
	{
		std::vector<float> p(3);
		p[0] = vec_positions.at(i).x;
		p[1] = vec_positions.at(i).y;
		p[2] = vec_positions.at(i).z;
		lp.push_back(p);
	}

	/*Miniball::Miniball<Miniball::CoordAccessor<std::list<std::vector<float>>::const_iterator, std::vector<float>::const_iterator>> miniball(3, lp.begin(), lp.end());
	glm::vec3 center = glm::vec3(miniball.center()[0], miniball.center()[1], miniball.center()[2]);
	m_boundingSphere = std::make_shared<Sphere>(sqrt(miniball.squared_radius()), 51, 51, glm::vec3(0.0f, 1.0f, 0.0f), center);*/
}

auto PointCloud::Render(GLenum type) const -> void
{	
	std::cout << "inside Render vec_positions.size()" << vec_indices.size() << std::endl;
	/*for (int i = 0; i < vec_indices.size(); i++)
	{
		std::cout << "inside Render vec_positions.size()" << vec_indices[i] << std::endl;
	}*/

	if (RenderIndices)
	{
		glBindVertexArray(vaoHandle);
			glDrawElements(type, static_cast<GLsizei>(vec_indices.size()), GL_UNSIGNED_INT, (void*)0); //default type should be GL_TRIANGLES
		glBindVertexArray(0);
	}
	else
	{
		glBindVertexArray(vaoHandle);
			glDrawArrays(type, 0, static_cast<GLsizei>(vec_positions.size())); //default type for pointclouds should be GL_points
		glBindVertexArray(0);
	}
}