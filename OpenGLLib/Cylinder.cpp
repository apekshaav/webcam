#pragma once


#include "stdafx.h"
#include "Cylinder.h"
/* **********************************************************************************
#  																					#
# Copyright (c) 2015-2016,															#
# Chair for Computer Aided Medical Procedures & Augmented Reality (CAMPAR) I-16		#
# Technische Universit�t M�nchen													#
# 																					#
# All rights reserved.																#
# Felix Bork - felix.bork@tum.de													#
# 																					#
# Redistribution and use in source and binary forms, with or without				#
# modification, are restricted to the following conditions:							#
# 																					#
#  * The software is permitted to be used internally only by CAMPAR and				#
#    any associated/collaborating groups and/or individuals.						#
#  * The software is provided for your internal use only and you may				#
#    not sell, rent, lease or sublicense the software to any other entity			#
#    without specific prior written permission.										#
#    You acknowledge that the software in source form remains a confidential		#
#    trade secret of CAMPAR and therefore you agree not to attempt to				#
#    reverse-engineer, decompile, disassemble, or otherwise develop source			#
#    code for the software or knowingly allow others to do so.						#
#  * Redistributions of source code must retain the above copyright notice,			#
#    this list of conditions and the following disclaimer.							#
#  * Redistributions in binary form must reproduce the above copyright notice,		#
#    this list of conditions and the following disclaimer in the documentation		#
#    and/or other materials provided with the distribution.							#
#  * Neither the name of CAMPAR nor the names of its contributors may be used		#
#    to endorse or promote products derived from this software without				#
#    specific prior written permission.												#
# 																					#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND	#
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED		#
# WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE			#
# DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR	#
# ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES	#
# (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;		#
# LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND		#
# ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT		#
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS		#
# SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.						#
# 																					#
*************************************************************************************/

Cylinder::Cylinder(float height, float radius, int segments, int slices, glm::vec3 color)
{	
	// Side
	float x{0.0f};
	float y{0.0f};
	float z{0.0f};   
	const float theta{2.0f * 3.14159265358979323846264338328f / static_cast<float>(segments)};
	float c{cosf(theta)};
	float s{sinf(theta)};
	float x2{radius};
	float z2{0};  
	float dslice = height / static_cast<float>(slices);
	
	for (int currentSlice = 0; currentSlice < slices; currentSlice++) 
	{
		for (int currentSegment = 0; currentSegment <= segments; currentSegment++) 
		{
			vec_vertices.push_back(glm::vec3(x + x2, y - height, z + z2));
			vec_vertices.push_back(glm::vec3(x + x2, y + dslice - height, z + z2));
			
			vec_normals.push_back(glm::vec3(x2 * 1./sqrtf(x2*x2+z2*z2), 0, z2 * 1./sqrtf(x2*x2+z2*z2)));

			vec_colors.push_back(color);
			vec_colors.push_back(color);

			vec_texcoords.push_back(glm::vec2(static_cast<float>(currentSegment) / segments, 0));
			vec_texcoords.push_back(glm::vec2(static_cast<float>(currentSegment) / segments, 1));

			// Next position
			const float x3 = x2;
			x2 = c * x2 - s * z2;
			z2 = s * x3 + c * z2;
		}
		
		y += dslice;
	}

	// Fill VAO
	glGenVertexArrays(1, &vaoHandle);
	glBindVertexArray(vaoHandle);
		// Positions
		vboPositionsHandle = GLUtils::BufferUtils::CreateBufferObject(GL_ARRAY_BUFFER, &vec_vertices[0], vec_vertices.size() * sizeof(glm::vec3), GL_STATIC_DRAW);
		glBindBuffer(GL_ARRAY_BUFFER, vboPositionsHandle);
			glEnableVertexAttribArray(0);
			glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, nullptr);
		glBindBuffer(GL_ARRAY_BUFFER, 0);
		// Normals
		vboNormalsHandle = GLUtils::BufferUtils::CreateBufferObject(GL_ARRAY_BUFFER, &vec_normals[0], vec_normals.size() * sizeof(glm::vec3), GL_STATIC_DRAW);
		glBindBuffer(GL_ARRAY_BUFFER, vboNormalsHandle);
			glEnableVertexAttribArray(1);
			glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 0, nullptr);
		glBindBuffer(GL_ARRAY_BUFFER, 0);
		// Colors
		vboColorsHandle = GLUtils::BufferUtils::CreateBufferObject(GL_ARRAY_BUFFER, &vec_colors[0], vec_colors.size() * sizeof(glm::vec3), GL_STATIC_DRAW);
		glBindBuffer(GL_ARRAY_BUFFER, vboColorsHandle);
			glEnableVertexAttribArray(2);
			glVertexAttribPointer(2, 3, GL_FLOAT, GL_FALSE, 0, nullptr);
		glBindBuffer(GL_ARRAY_BUFFER, 0);
		// Texture Coords
		vboTextCoordsHandle = GLUtils::BufferUtils::CreateBufferObject(GL_ARRAY_BUFFER, &vec_texcoords[0], vec_texcoords.size() * sizeof(glm::vec2), GL_STATIC_DRAW);
		glBindBuffer(GL_ARRAY_BUFFER, vboTextCoordsHandle);
			glEnableVertexAttribArray(3);
			glVertexAttribPointer(3, 2, GL_FLOAT, GL_FALSE, 0, nullptr);
		glBindBuffer(GL_ARRAY_BUFFER, 0);
	glBindVertexArray(0);
}

Cylinder::~Cylinder(void)
{
	glDeleteVertexArrays(1, &vaoHandle);

	GLUtils::BufferUtils::DeleteBufferObject(&vboPositionsHandle);
	GLUtils::BufferUtils::DeleteBufferObject(&vboNormalsHandle);
	GLUtils::BufferUtils::DeleteBufferObject(&vboColorsHandle);
	GLUtils::BufferUtils::DeleteBufferObject(&vboTextCoordsHandle);
}

auto Cylinder::Render(GLenum type) const -> void
{
	glBindVertexArray(vaoHandle);

	glDrawArrays(type, 0, static_cast<GLsizei>(vec_vertices.size()));

	glBindVertexArray(0);
}