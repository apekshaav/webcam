
#pragma once 

#include "stdafx.h"
#include <glm/glm.hpp>
#include <vector>

struct PLYModel 
{
	std::vector<glm::vec3> positions;
	std::vector<glm::vec3> normals;
	std::vector<glm::vec3> colors;
	std::vector<glm::vec3> faces;
	std::vector<unsigned int> vec_indices;
	/*glm::vec3 *positions;
	glm::vec3 *normals;
	glm::vec3 *colors;*/

	int vertexCount; //number of vertices
	float bvWidth, bvHeight, bvDepth; //bounding volume dimensions
	float bvAspectRatio; //bounding volume aspect ratio
	int faceCount; //number of faces; if reading meshes
	bool isMesh; // To tell if this is a mesh or not
	bool ifColor,ifNormal;

	glm::vec3 min, max, center;

	//PLYModel();
	PLYModel(const char *filename, bool isNormal, bool isColor); //To indicate if normal, color informations are present in the file respectively
	void PLYWrite(const char *filename, bool isNormal, bool isColor);// To indicate if normal, color informations are to be written in the file respectively
	void FreeMemory();
};

struct Material {
	glm::vec4 Ka;
	glm::vec4 Kd;
	glm::vec4 Ks;
	float shininess;
};

struct Light {
	glm::vec4 La;
	glm::vec4 Ld;
	glm::vec4 Ls;
	glm::vec4 position;
};


