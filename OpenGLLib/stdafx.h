// stdafx.h : include file for standard system include files,
// or project specific include files that are used frequently, but
// are changed infrequently
//

#pragma once

#include "targetver.h"

#define WIN32_LEAN_AND_MEAN             // Exclude rarely-used stuff from Windows headers
// Windows Header Files:
#include <afx.h> //in afx.h windows.h is included (perhaps)
//#include <windows.h>

#define OPENGLLIB_DLL_EXPORTS

#ifdef OPENGLLIB_DLL_EXPORTS
#define OPENGLLIB_API __declspec(dllexport)   
#else  
#define OPENGLLIB_API __declspec(dllimport)   
#endif 

// TODO: reference additional headers your program requires here