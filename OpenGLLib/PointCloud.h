#pragma once

#include <glm/glm.hpp> 
#include <glm/gtc/type_ptr.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include "GL\glew.h"
#include "GLUtils.h"
#include <vector>
#include "Drawable.h"
#include "Sphere.h"
#include "plyloader.h"

/* **********************************************************************************
#  																					#
# Copyright (c) 2015-2016,															#
# Chair for Computer Aided Medical Procedures & Augmented Reality (CAMPAR) I-16		#
# Technische Universit�t M�nchen													#
# 																					#
# All rights reserved.																#
# Felix Bork - felix.bork@tum.de													#
# 																					#
# Redistribution and use in source and binary forms, with or without				#
# modification, are restricted to the following conditions:							#
# 																					#
#  * The software is permitted to be used internally only by CAMPAR and				#
#    any associated/collaborating groups and/or individuals.						#
#  * The software is provided for your internal use only and you may				#
#    not sell, rent, lease or sublicense the software to any other entity			#
#    without specific prior written permission.										#
#    You acknowledge that the software in source form remains a confidential		#
#    trade secret of CAMPAR and therefore you agree not to attempt to				#
#    reverse-engineer, decompile, disassemble, or otherwise develop source			#
#    code for the software or knowingly allow others to do so.						#
#  * Redistributions of source code must retain the above copyright notice,			#
#    this list of conditions and the following disclaimer.							#
#  * Redistributions in binary form must reproduce the above copyright notice,		#
#    this list of conditions and the following disclaimer in the documentation		#
#    and/or other materials provided with the distribution.							#
#  * Neither the name of CAMPAR nor the names of its contributors may be used		#
#    to endorse or promote products derived from this software without				#
#    specific prior written permission.												#
# 																					#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND	#
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED		#
# WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE			#
# DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR	#
# ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES	#
# (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;		#
# LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND		#
# ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT		#
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS		#
# SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.						#
# 																					#
*************************************************************************************/

class PointCloud : public Drawable
{
public:
	PointCloud(const char *filename, bool normal, bool color);
	PointCloud(const char *filename, bool normal, bool color, bool Faces);
	//explicit PointCloud();
	//explicit PointCloud(std::vector<glm::vec3> positions); //for point clouds 
	virtual ~PointCloud();

	//getters 
	auto GetAABB()           const->std::vector<glm::vec3>;
	auto GetBoundingSphere() const -> const std::shared_ptr<Sphere>;
	auto GetVertices() -> std::vector<glm::vec3> ; 
	auto GetIndices()-> std::vector<unsigned int>;
	auto GetColors() -> std::vector<glm::vec3>;
	auto GetNormamls() -> std::vector<glm::vec3>;
	
	//auto SetPrimaryTexture(std::shared_ptr<Texture>& texture) -> void;
	auto CalculateNewRange(std::vector<glm::vec3> RequiredScaleRange, std::vector<glm::vec3> CurrentRange) -> glm::mat4; //Brings dim from one range to another 
	auto CalculateBoundingSphere() -> void;
	auto Render(GLenum type) const -> void override;
	//auto RenderPoints(GLenum type) const -> void; //for point clouds  //type will be GL_point 

private:
	std::string m_name{ "" };

	GLuint vaoHandle{ 0 };
	GLuint vboPositionsHandle{ 0 };
	GLuint vboNormalsHandle{ 0 };
	GLuint vboTextCoordsHandle{ 0 };
	GLuint vboIndicesHandle{ 0 };
	GLuint vboColorHandle{ 0 };

	std::vector<glm::vec3> vec_positions;
	//std::vector<glm::vec3> faceIndexes;
	std::vector<glm::vec3> vec_normals;
	std::vector<glm::vec3> vec_colors;
	std::vector<glm::vec2> vec_texcoords;
	std::vector<unsigned int> vec_indices;

	glm::vec3 m_centroid;
	std::shared_ptr<Sphere> m_boundingSphere{ nullptr };
	glm::vec3 m_sideLength{ glm::vec3() };
	std::vector<glm::vec3> vec_aabb{};
	std::vector<glm::vec3> m_costumaabb{};
	int m_vertexOffset{ 0 };

	//ply 
	std::shared_ptr<PLYModel> PLYPointCloud{ nullptr };
	//point cloud 
	std::vector<glm::vec3> range;
	bool RenderIndices = false; 
	/*PLYModel PLYPointCloud;
	std::shared_ptr<PLYModel> PLYPointCloud = std::make_shared<PLYModel>();*/
};